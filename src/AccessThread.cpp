static const char *RcsId = "$Header: /cvsroot/tango-ds/Communication/DataSocket/src/AccessThread.cpp,v 1.7 2012/12/03 11:04:21 buteau Exp $";
//+=============================================================================
//
// file :	  AccessThread.cpp
//
// description :  C++ source for tools used by the Starter device server.
//
// project :	  TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.7 $
//
//
// copyleft :	  European Synchrotron Radiation Facility
//		  BP 220, Grenoble 38043
//		  FRANCE
//
//-=============================================================================
#include <tango.h>

#include "dataskt.h"
#include <cvirte.h>	/* Needed if linking in external compiler; harmless otherwise */
#include <utility.h>  
#include <AccessThread.h>

#include <iostream>
// timeout for opening datasockets 
const int TIMEOUT=2000;



//+=========================================================================
//
//		Access Thread Class
//
//+=========================================================================

//+==============================================
/**
*	Thread Constructor
*/
//+==============================================
AccessThread::AccessThread(Tango::DeviceImpl* _dev,SharedData *shm, omni_semaphore* _empty_sem, omni_semaphore* _full_sem, omni_condition* _work_to_do)
:
Tango::LogAdapter(_dev),
is_started_(false),
stop_(false),
shared(shm),
empty_sem_(_empty_sem),
full_sem_(_full_sem),
work_to_do_(_work_to_do),
free_all_(false)
{
}
//+==============================================
/**
*	Stop the thread
*/
//+==============================================
void AccessThread::stop(void)
{
	if(!stop_)
		stop_ = true; 
}
//+==============================================
/**
*	
*/
//+==============================================
bool AccessThread::is_started(void)
{
	return this->is_started_; 
}
//+==============================================
/**
*	Execute the thread loop.
*/
//+==============================================
void * AccessThread::run_undetached(void *ptr)
{
	// Init CVI RunTime	
	if (InitCVIRTE (0, 0, 0) == 0)
	{
		ERROR_STREAM << "InitCVIRTE::DataSocket() create device: Error initializing InitCVIRTE" << std::endl;
		shared-> cvi_runtime_error=true;
	}
	else
	{
		DEBUG_STREAM  << "InitCVIRTE::OK, thread id = " << CmtGetCurrentThreadID() <<  std::endl;
		shared-> cvi_runtime_error=false;
	}

	is_started_ = true;

	// Loop while not stopped, waiting for handles to open 	
	while(!stop_)
	{
		DEBUG_STREAM << " A) Waiting for handles to open " <<  endl ;

		if(stop_) break;

		// 1) Wait for data
		full_sem_->wait();

		if(stop_) break;

		DEBUG_STREAM << "B) Some Work arrived "<<  endl ;  
		{
			// Protect access to SharedData
			omni_mutex_lock sync_consumer(*shared);

			// open handles only if close_all_handles not requested
			if(!free_all_)
			{
				if (shared-> read_url != "")
				{
					// cout << "opening read handle=" <<shared-> read_url.c_str()<< std::endl;
					//TODO : DSConst_ReadAutoUpdate  :: Configures the DataSocket object for automatic updating. 
					//                               The DataSocket object gets data from the DataSocket data source at the time that the connection is established, when the data value changes, and when any of the data attributes change. 
					//                               This mode is valid only for DSTP and Logos and OPC data sources.

					// DSConst_ReadBuffered ::Configures the DataSocket object for manual updating with buffering.
					//  The DataSocket object gets the next data in the buffer from the DataSocket data source at the time that the connection is established and when you call DS_Update.

					// DSConst_ReadBufferedAutoUpdate::   Configures the DataSocket object for automatic updating with buffering. 
					// T                               he DataSocket object gets the data from the DataSocket data source at the time that the connection is established, when the data value changes, and when any of the data attributes change.
					//					shared->status= DS_Open (shared-> read_url.c_str(),DSConst_ReadAutoUpdate, NULL, NULL, shared-> read_dshandle);
					
				INFO_STREAM << " AccessThread::run_undetached creation of read url <" << shared-> read_url << ">" << endl;

				shared->status= DS_OpenEx (shared-> read_url.c_str(),DSConst_ReadAutoUpdate, NULL, NULL,DSConst_PollingModel, TIMEOUT,shared-> read_dshandle);
				INFO_STREAM << " AccessThread::run_undetached created read url <" << shared-> read_url << ">" << endl;

					// Reset mailbox field
				shared-> read_url = "";
				}

				// get url to open for writing
				if (shared-> write_url != "")
				{
					//cout << "opening write handle=" <<shared-> write_url.c_str()<< endl;
					//shared-> status = DS_Open (shared-> write_url.c_str(), DSConst_Write, NULL, NULL, shared-> write_dshandle);
				INFO_STREAM << " AccessThread::run_undetached creation of write url <" << shared-> write_url << ">" << endl;
					shared->status= DS_OpenEx (shared-> write_url.c_str(),DSConst_Write, NULL, NULL,DSConst_PollingModel, TIMEOUT,shared-> write_dshandle);
			INFO_STREAM << " AccessThread::run_undetached created write url <" << shared-> write_url << ">" << endl;

					// Reset mailbox field
					shared-> write_url = "";

				}	
			}
			// close handle
			if (shared-> dshandle_to_close != 0)
			{
				DS_DiscardObjHandle (*(shared-> dshandle_to_close) );
				// wait a little bit before returning
				Sleep(100);
				INFO_STREAM << "C bis) One handle closed" << endl;
				shared-> dshandle_to_close = 0;
			}
		}  // delete of sync_consumer


		// End of work
		DEBUG_STREAM << "D) Work done : requested handles opened and closed" << endl ;

		DEBUG_STREAM << "E) signal that data has been consumed" <<  endl ;
		// 2) signal that data has been consumed
		empty_sem_->post();

		if(stop_) break;
	} //end while

	DEBUG_STREAM << "Thread End" <<  endl <<endl ;
	return 0;
}

