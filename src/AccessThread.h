//=============================================================================
//
// file :        AccessThread.h
//
// description : Include for the AccessThread class.
//
// project :	OPC access.
//
// $Author: buteau $
//
// $Revision: 1.3 $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
#ifndef _access_thread_H
#define _access_thread_H

#include <string>
#include "tango.h"
#include "SharedData.h"


/**
*	Create a thread to read values
*	because all access must be done by the same thread.
*/
class AccessThread: public omni_thread, public Tango::LogAdapter
{
private:
	SharedData		*shared;
	omni_condition* work_to_do_;

	omni_semaphore* empty_sem_;
	omni_semaphore* full_sem_;
	bool stop_;
	bool is_started_;

public:
	void set_free_all(bool value) {free_all_= value;};
	bool get_free_all() {return free_all_;};
	void stop(void);
	bool is_started(void);
	void start() {start_undetached();}
	/**
	*	Create a thread to read values 
	*	because all access must be done by the same thread.
	*/
	AccessThread(Tango::DeviceImpl* dev, SharedData *shm, omni_semaphore* _empty_sem, omni_semaphore* _full_sem, omni_condition* _work_to_do);

private:
	/**
	*	Execute the thread loop.
	*/
	void *run_undetached(void *);
	bool free_all_;

};

#endif	// _access_thread_H

