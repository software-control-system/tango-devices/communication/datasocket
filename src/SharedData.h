#include "dataskt.h"
#include <tango.h>

#ifndef _SHAREDDATA_H
#define _SHAREDDATA_H

/**
*	Shared data between DS and CVI thread.
*/
class SharedData: public Tango::TangoMonitor
{
public:

	/**
	*	Constructor
	*/
	SharedData() { 
	read_url="";
	write_url="",
	read_dshandle=0; 
	write_dshandle=0; 
	dshandle_to_close=0;
	cvi_runtime_error=false;
	status=0;
	};
	string read_url;
	DSHandle* read_dshandle;

	string write_url;
	DSHandle* write_dshandle;

	DSHandle* dshandle_to_close;
	bool cvi_runtime_error;
	HRESULT status ; // CVI Open/Close status
private:
};

#endif 